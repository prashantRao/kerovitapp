/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import Routes from './components/routes'
import SplashScreen from 'react-native-splash-screen'
import { AppProvider } from './components/context/AppContext';

class App extends React.Component{
  componentDidMount(){
    SplashScreen.hide()
  }
  render(){
    return(
      <AppProvider>
        <Routes />
      </AppProvider>      
    )
  }
}

export default App;