import React from 'react'
import { View, StyleSheet, Dimensions, ScrollView, TouchableHighlight, Text, ImageBackground, Animated, Image } from 'react-native'
import { Easing } from 'react-native-reanimated';
import LinearGradient from 'react-native-linear-gradient';
import { apiPath } from './constants'
import { AppContext } from './context/AppContext';

class Series extends React.Component {
    static contextType = AppContext;
    
    constructor() {
        super()
        this.state = {
            filterOn: false,
            startAnimate: new Animated.Value(0),
            animatedValue: new Animated.Value(-50),
            textVisible: false,
            seriesData: [],
            dataLoading: true,
            category: ''
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <TouchableHighlight style={styles._menu} onPress={() => navigation.goBack()} underlayColor="transparent">
                <Image source={require('./../assets/images/back_icon.png')} style={{ width: 18, height: 18 }} />
            </TouchableHighlight>
        }
    }

    componentDidMount = () => {
        var categoryId = this.props.navigation.getParam('category');
        this._getSeriesData(categoryId);
        this.setState({
            category: categoryId
        })
        this.startAnimation();

        this.props.navigation.addListener('willFocus', payload => {
            this.setState({
                startAnimate: new Animated.Value(0)
            })
        })
    }

    _openFilter() {
        this.setState({
            filterOn: !this.state.filterOn
        }, () => {
            Animated.timing(
                this.state.startAnimate,
                {
                    toValue: this.state.filterOn ? 250 : 0,
                    duration: 150,
                    easing: Easing.linear,
                    delay: 0
                }
            ).start()
        })
    }

    startAnimation() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animatedValue, {
                    toValue: 220,
                    duration: 1500
                })
            ]),
            {
                iterations: 10
            }
        ).start()
    }

    _getSeriesData(categoryName) {
        fetch(`${apiPath}/getSeries`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                cat_name: categoryName
            })
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    seriesData: res.data
                }, () => {
                    this.setState({
                        dataLoading: false
                    })
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    startAnimation() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animatedValue, {
                    toValue: 450,
                    duration: 800
                })
            ]),
            {
                iterations: 10
            }
        ).start()
    }

    _gotoProducts(result) {
        this.props.navigation.navigate('Products', { series: result.series, image: result.image, category: this.state.category })
    }

    _goToDetail(result) {
        this.props.navigation.navigate('ProductDetail', { productCode: result.product_code })
    }

    _goToSeriesList() {
        this.props.navigation.navigate('SeriesList', { categoryName: this.state.category })
    }

    _goToTypeList() {
        this.props.navigation.navigate('TypeList', { categoryName: this.state.category })
    }


    render() {
        var seriesData = '';
        if (this.state.dataLoading == true) {
            seriesData =
                <View style={styles._loaderWrap}>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                </View>
        } else {
            seriesData = this.state.seriesData.map((result, index) => {
                if (result.category_name === 'Basin' || result.category_name === 'Toilet') {
                    return (
                        <TouchableHighlight onPress={() => this._goToDetail(result)} underlayColor="transparent" key={index}>
                            <View style={styles._imgWrap}>
                                <View style={{ minHeight: 250, backgroundColor: '#f7f7f7' }}>
                                    <Image source={{ uri: result.image, width: Dimensions.get('window').width - 30, height: 320 }} style={{ resizeMode: 'cover' }} resizeMethod="resize" />
                                </View>
                                <Text style={[styles._catName]}>{result.series}</Text>
                            </View>
                        </TouchableHighlight>
                    )
                } else {
                    return (
                        <TouchableHighlight onPress={() => this._gotoProducts(result)} underlayColor="transparent" key={index}>
                            <View style={styles._imgWrap}>
                                <View style={{ minHeight: 250, backgroundColor: '#f7f7f7' }}>
                                    <Image source={{ uri: result.image, width: Dimensions.get('window').width - 30, height: 320 }} style={{ resizeMode: 'cover' }} resizeMethod="resize" />
                                </View>
                                <Text style={[styles._catName]}>{result.series}</Text>
                            </View>
                        </TouchableHighlight>
                    )
                }
            })
        }
        return (
            <View>
                <ImageBackground source={require('./../assets/images/plane_bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode="cover">
                    <ScrollView>
                        <View style={styles._catWrap}>
                            {seriesData}
                        </View>
                    </ScrollView>
                    {this.state.typeId ? <></> :
                        <View style={styles._btmOptions}>
                            <Animated.View style={[styles._filterOpt, { width: this.state.startAnimate }]}>
                                <TouchableHighlight style={[styles._btnWrap]} onPress={() => this._goToSeriesList()}>
                                    <View style={styles._btnInner}>
                                        <Text style={styles._btnTxt}>{`Search by\nSeries`}</Text>
                                    </View>
                                </TouchableHighlight>
                                <View style={{ justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 20 }}>|</Text>
                                </View>
                                <TouchableHighlight style={styles._btnWrap} onPress={() => this._goToTypeList()}>
                                    <View style={styles._btnInner}>
                                        <Text style={styles._btnTxt}>{`Search by\ntype`}</Text>
                                    </View>
                                </TouchableHighlight>
                            </Animated.View>
                            <TouchableHighlight style={styles._filterBtn} onPress={() => this._openFilter()}>
                                <Image source={require('./../assets/images/filter_icon_large.png')} style={{ width: 20, height: 20 }} />
                            </TouchableHighlight>
                        </View>
                    }
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _catWrap: {
        padding: 15
    },
    _btmOptions: {
        position: 'absolute', bottom: 15, flexDirection: 'row', justifyContent: 'flex-end', right: 15, left: 15
    },
    _btnWrap: {
        width: '48%'
    },
    _btnTxt: {
        color: 'white', fontSize: 11, marginLeft: 6, fontFamily: 'SinkinSans-300Light', textAlign: 'center', lineHeight: 17, height: 35
    },
    _btnInner: {
        flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
    },
    _imgWrap: {
        position: 'relative'
    },
    _catName: {
        color: '#2e3b4e', fontSize: 22, textTransform: 'uppercase', fontFamily: 'SinkinSans-700Bold', zIndex: 5, backgroundColor: 'white', textAlign: 'center', lineHeight: 32, paddingTop: 10, paddingBottom: 8, paddingHorizontal: 15
    },
    _filterBtn: {
        width: 50, height: 50, backgroundColor: '#182132', borderRadius: 25, justifyContent: 'center', alignItems: 'center', elevation: 8
    },
    _filterOpt: {
        justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'rgba(24,33,50,0.8)', borderTopLeftRadius: 125, borderBottomLeftRadius: 125, paddingRight: 10, paddingLeft: 0, marginRight: -22, height: 50, overflow: 'hidden'
    },
    _gradientWrap: {
        position: 'absolute', bottom: 0, left: 0, right: 0, height: 50, zIndex: 1
    },
    _loadWrapper: {
        width: '100%', height: 350, backgroundColor: 'black', position: 'relative', overflow: 'hidden', marginBottom: 15
    },
    _loadShine: {
        position: 'absolute', top: -20, bottom: -20, width: 80, backgroundColor: 'white', transform: [{ rotate: '10deg' }]
    },
    _loaderWrap: {
        width: '100%'
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    }
})

export default Series;