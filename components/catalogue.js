import React from 'react'
import { WebView } from 'react-native-webview';

function Catalogue(){
    return(
        <WebView
            source={{uri: 'https://www.kerovit.com/catalogue/'}}
            style={{backgroundColor: '#003353'}}
        />
    )
}

export default Catalogue;