import React from 'react'
import { View, Text, Animated, Easing, TouchableHighlight } from 'react-native'

export default class Animation extends React.Component {
    constructor() {
        super()
        this.state = {
            filterOn: false,
            startValue: new Animated.Value(0)
        }
    }

    animateItem() {
        this.setState({
            filterOn: !this.state.filterOn
        }, () => {
            Animated.timing(
                this.state.startValue,
                {
                    toValue: this.state.filterOn ? 200 : 0,
                    duration: 250,         // in milliseconds, default is 500
                    easing: Easing.ease, // Easing function, default is Easing.inOut(Easing.ease)
                    delay: 0,
                }
            ).start()
        })
    }

    render() {
        return (
            <View>
                <View>
                    <Animated.View style={{ width: this.state.startValue, height: 100, borderRadius: 50, backgroundColor: 'black' }} />
                </View>
                <View>
                    <TouchableHighlight style={{ backgroundColor: 'black', padding: 15, marginTop: 20 }} onPress={() => this.animateItem()}>
                        <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }}>Click Me</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}