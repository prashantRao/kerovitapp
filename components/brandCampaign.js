import React from 'react'
import { View, Text, Dimensions, ActivityIndicator, FlatList, StyleSheet, ScrollView, TouchableHighlight, Image } from 'react-native'
import { apiPath } from './constants'
import Video from 'react-native-video';
import { withNavigation } from 'react-navigation'
import { WebView } from 'react-native-webview'
import Share from 'react-native-share'
import { AppContext } from './context/AppContext';

class BrandCampaign extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props)
        this.state = {
            pageData: []
        }
    }

    componentDidMount() {
        this._getWhatsNew()
    }

    _getWhatsNew() {
        fetch(`${apiPath}/newsList`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            }
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    pageData: res.data
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    _newsDetail(id) {
        this.props.navigation.navigate('NewsDetail', { newsId: id })
    }

    _shareCampaign(item){
        Share.open({
            message: item.title,
            url: item.video_url,
        });
    }

    renderItem = ({ item }) => {
        var updateDate = item.created_at.split(' ')[0];
        if (item.type === 'brand_campaign') {
            const OriginalVideo = item.video_url
            const SplitedVideo = OriginalVideo.split("watch?v=")
            const Embed = SplitedVideo.join("embed/")
            return (
                <View style={{marginBottom: 20}}>
                    <WebView
                        style={{ width: Dimensions.get('window').width - 30, height: 250 }}
                        domStorageEnabled={true}
                        source={{ uri: Embed }}
                    />
                    <View style={styles._contentWrap}>
                        <View style={styles._leftWrap}>
                            <Text style={styles._newsDate}>{updateDate}</Text>
                            <Text style={styles._titleText}>{item.title}</Text>
                        </View>
                        <View>
                            <TouchableHighlight style={styles._searchIcon} underlayColor="#b7b7b7" onPress={() => this._shareCampaign(item)}>
                                <Image source={require('./../assets/images/shareIcon_black.png')} style={{height: 16, width: 16}}/>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            )
        }
    }

    render() {
        var pageContent = ''
        if (this.state.pageData.length != 0) {
            pageContent = <FlatList data={this.state.pageData} renderItem={this.renderItem} style={styles._wrapper} />
        } else {
            pageContent = <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size={40} color="white" /></View>
        }

        return (
            <View style={{ backgroundColor: 'black', height: '100%', width: '100%' }}>                
                {pageContent}                         
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _wrapper: {
        padding: 15
    },
    _titleText: {
        color: 'black', fontFamily: 'SinkinSans-400Regular', fontSize: 14, lineHeight: 22
    },
    _newsWrap: {
        marginBottom: 20
    },
    _contentWrap: {
        backgroundColor: 'white', padding: 10, flexDirection: 'row'
    },
    _newsDate: {
        fontFamily: 'SinkinSans-400Regular', fontSize: 10, marginBottom: 5
    },
    _leftWrap: {
        flex: 1, marginRight: 20
    },
    _searchIcon: {
        width: 36, height: 36, backgroundColor: '#d9d9d9', borderRadius: 23, alignItems: 'center', justifyContent: 'center'
    }
})

export default withNavigation(BrandCampaign);