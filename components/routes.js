import React from 'react'
import { View, Image, TouchableHighlight, Text, StyleSheet, ScrollView, SafeAreaView, Linking } from 'react-native'
import Series from './series'
import Category from './category'
import Products from './products'
import Dealers from './dealers'
import About from './about'
import DisplayCenter from './displayCenter'
import QueryForm from './queryForm'
import ProductDetail from './productDetail'
import Support from './support'
import Catalogue from './catalogue'
import Faq from './faq'
import Search from './search'
import Whatsnew from './whatsnew'
import NewsDetail from './newsDetail'
import SeriesList from './seriesList'
import TypeList from './typeList'
import FilterPage from './filterPage'
import IntroPage from './introPage';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';

const Navicon = (props) => {
    return (
        <TouchableHighlight style={styles._menu} onPress={() => props.navigate.openDrawer()} underlayColor="transparent">
            <Image source={require('./../assets/images/nav_icon.png')} style={{ width: 20, height: 18 }} />
        </TouchableHighlight>
    )
}

const Logo = (props) => {
    return (
        <View style={{ flex: 1 }}>
            <TouchableHighlight onPress={() => props.navigate.navigate('Category')} underlayColor="transparent">
                <Image source={require('./../assets/images/logo.png')} style={{ width: 60, height: 29, alignSelf: 'center' }} />
            </TouchableHighlight>
        </View>
    )
}

const Searchicon = (props) => {
    return (
        <TouchableHighlight style={styles._search} onPress={() => props.navigate.navigate('Search')}>
            <Image source={require('./../assets/images/search_icon.png')} style={{ width: 18, height: 18 }} />
        </TouchableHighlight>
    )
}

const Routes = createStackNavigator({
    Series: {
        screen: Series
    },
    Category: {
        screen: Category
    },
    Products: {
        screen: Products
    },
    ProductDetail: {
        screen: ProductDetail
    },
    About: {
        screen: About
    },
    DisplayCenter: {
        screen: DisplayCenter
    },
    QueryForm: {
        screen: QueryForm
    },
    Support: {
        screen: Support
    },
    Catalogue: {
        screen: Catalogue
    },
    Faq: {
        screen: Faq
    },
    Search: {
        screen: Search
    },
    Whatsnew: {
        screen: Whatsnew
    },
    NewsDetail: {
        screen: NewsDetail
    },    
    Dealers: {
        screen: Dealers
    },
    SeriesList: {
        screen: SeriesList
    },
    TypeList: {
        screen: TypeList
    },
    FilterPage: {
        screen: FilterPage
    },
    IntroPage: {
        screen: IntroPage,
        navigationOptions: {
            headerShown: false
        }
    }
}, {
    defaultNavigationOptions: ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: 'black',
                elevation: 0,
                height: 60
            },
            headerLeft: <Navicon navigate={navigation} />,
            headerTitle: (
                <Logo navigate={navigation}/>
            ),
            headerRight: (
                <Searchicon navigate={navigation}/>
            ),
        }
    },
    initialRouteName: 'IntroPage'
});

_navigatePage = (page, navigation) => {
    navigation.navigate(page);
    navigation.closeDrawer();
}

const Drawer = createDrawerNavigator({
    Products: {
        screen: Routes
    }
}, {
    drawerType: 'front',
    drawerBackgroundColor: 'transparent',
    contentOptions: {
        activeTintColor: 'white',
        activeBackgroundColor: 'transparent',
        inactiveTintColor: '#7e7e7e',
        inactiveBackgroundColor: 'transparent',
        labelStyle: {
            fontSize: 13,
            marginLeft: 15,
            fontWeight: 'normal',
            fontFamily: 'SinkinSans-400Regular'
        }
    },
    initialRouteName: 'Products',
    contentComponent: ({navigation}) => {        
        return (
            <View style={{ backgroundColor: 'rgba(24,33,50,0.8)', marginTop: 60, height: '100%' }}>
                <ScrollView>
                    <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
                        <View>
                            <TouchableHighlight style={styles._btnParent} onPress={() => _navigatePage('About', navigation)}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/about.png')} style={styles._navIcons}/>
                                    <Text style={styles._navTxt}>About</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles._btnParent} onPress={() => _navigatePage('Catalogue', navigation)}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/catalogue.png')} style={[styles._navIcons, {height: 20}]}/>
                                    <Text style={styles._navTxt}>Catalogue</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles._btnParent} onPress={() => _navigatePage('FilterPage', navigation)}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/wheretobuy.png')} style={styles._navIcons}/>
                                    <Text style={styles._navTxt}>Where to buy</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles._btnParent} onPress={() => _navigatePage('DisplayCenter', navigation)}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/displaycenter.png')} style={[styles._navIcons, {height: 16}]}/>
                                    <Text style={styles._navTxt}>Display Center</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles._btnParent} onPress={() => _navigatePage('Whatsnew', navigation)}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/whatsnew.png')} style={styles._navIcons}/>
                                    <Text style={styles._navTxt}>What's New</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles._btnParent} onPress={() => Linking.openURL('https://www.kerovit.com/blog/')}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/blog.png')} style={styles._navIcons}/>
                                    <Text style={styles._navTxt}>Blog</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight style={[styles._btnParent]} onPress={() => _navigatePage('Faq', navigation)}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/faq.png')} style={styles._navIcons}/>
                                    <Text style={styles._navTxt}>FAQ's</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight style={[styles._btnParent, {borderBottomWidth: 0}]} onPress={() => _navigatePage('Support', navigation)}>
                                <View style={styles._btnWrap}>
                                    <Image source={require('./../assets/images/nav_icons/support.png')} style={styles._navIcons}/>
                                    <Text style={styles._navTxt}>Support</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        )
    }
})

const styles = StyleSheet.create({
    _search: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    _navIcons: {
        width: 18, height: 18
    },
    _navTxt: {
        fontSize: 12, marginLeft: 15, fontWeight: 'normal', fontFamily: 'SinkinSans-400Regular', color: 'white'
    },
    _btnWrap: {
        flexDirection: 'row', alignItems: 'center'
    },
    _btnParent: {
        paddingHorizontal: 15, borderBottomWidth: 1, borderColor: 'rgba(255,255,255,0.5)', paddingVertical: 20
    }
})

export default createAppContainer(Drawer);