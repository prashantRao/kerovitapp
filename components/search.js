import React from 'react'
import {View, Text, FlatList, TouchableHighlight, StyleSheet, TextInput, ActivityIndicator, ImageBackground, Linking} from 'react-native'
import Image from 'react-native-scalable-image';
import {apiPath} from './constants'
import { AppContext } from './context/AppContext';

var global = this;

class Search extends React.Component{  
    static contextType = AppContext;

    static navigationOptions = ({navigation}) => {
        return{
            header: null
        }
    }
    
    constructor(){
        super()
        this.state = {
            searchKey: '',
            searchResult: [],
            dataLoading: false,
            mode: ''
        }
    }

    componentDidMount(){
        var searchMode = this.props.navigation.getParam('searchMode')
        if(searchMode == 'dealer'){
            this.setState({
                mode: searchMode
            })
        } else if(searchMode == 'displayCenter') {
            this.setState({
                mode: 'displayCenter'
            })
        } else {
            this.setState({
                mode: 'product'
            })
        }
    }

    _searchResult = () => {
        this._startSearch()    
    }

    _startSearch(){
        this.setState({
            dataLoading: true
        })
        fetch(`${apiPath}/search`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                key: this.state.searchKey,
                mode: this.state.mode
            })
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                searchResult: res.data
            }, () => {
                this.setState({
                    dataLoading: false
                })
            })
        })
    }

    _goToDetail(result){
        this.props.navigation.navigate('ProductDetail', {productCode: result.product_model})
    }

    _renderItem = ({item, index}) => {
        var checkEven = index % 2 == 0;
        if(this.state.mode == 'product'){
            return(
                <View style={[styles._innerProd, {marginRight: checkEven ? '4%' : 0}]}>
                    <TouchableHighlight onPress={() => this._goToDetail(item)} underlayColor="transparent">
                        <View style={{display: 'flex', alignItems: 'center', paddingHorizontal: 10}}>
                            <Image source={{uri: item.image}} width={110}/>
                            <Text style={styles._prodName}>{`Model No:\n${item.product_model}`}</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            )
        } else if(this.state.mode == 'dealer'){
            var updatedContent = item.contactnumber.split('/')[0];
            return(
                <View style={[styles._secWrap, checkEven ? styles._evenWrap : <></>]}>
                    <View style={styles._nameWrap}>
                        <Text style={[styles._nameTxt, checkEven ? styles._whiteColor : <></>]}>{item.dealername}</Text>
                        <Text style={[styles._smlDec, checkEven ? styles._whiteColor : <></>]}>{item.address1+' '+item.address2+' '+item.address3+' '+item.suburbs+' '+item.city+', '+item.state+' - '+item.pincode}</Text>
                    </View>
                    <TouchableHighlight style={styles._whtBtn} underlayColor="transparent" onPress={() => Linking.openURL(`sms:${updatedContent}`)}>
                        {checkEven ? 
                            <Image source={require('./../assets/images/message_white.png')} width={22} />
                            :
                            <Image source={require('./../assets/images/message_color.png')} width={22} />
                        }
                    </TouchableHighlight>
                    <TouchableHighlight style={styles._phoneBtn} underlayColor="transparent" onPress={() => Linking.openURL(`tel: ${item.contactnumber}`)}>
                        {checkEven ? 
                            <Image source={require('./../assets/images/phone_white.png')} width={22} />
                            :
                            <Image source={require('./../assets/images/phone_color.png')} width={22}/>
                        }
                    </TouchableHighlight>
                </View>
            )
        } else {
            var updatedContent = item.contactnumber.split('/')[0];
            return(
                <View style={[styles._secWrap, checkEven ? styles._evenWrapSecond : <></>]}>
                    <View style={styles._nameWrap}>
                        <Text style={[styles._nameTxt, checkEven ? styles._whiteColor : <></>]}>{item.dealername}</Text>
                        <Text style={[styles._smlDec, checkEven ? styles._whiteColor : <></>]}>{item.address1+' '+item.address2+' '+item.address3+' '+item.suburbs+' '+item.city+', '+item.state+' - '+item.pincode}</Text>
                        <TouchableHighlight onPress={() => Linking.openURL(item.google_link)} style={styles._directionBtn} underlayColor="white">
                            <Text style={[styles._btnTxt, {color: 'black', fontSize: 11}]}>Directions</Text>
                        </TouchableHighlight>
                    </View>
                    <TouchableHighlight style={styles._whtBtn} underlayColor="transparent" onPress={() => Linking.openURL(`sms:${updatedContent}`)}>
                        {checkEven ? 
                            <Image source={require('./../assets/images/message_white.png')} width={22} />
                            :
                            <Image source={require('./../assets/images/message_color.png')} width={22} />
                        }
                    </TouchableHighlight>
                    <TouchableHighlight style={styles._phoneBtn} underlayColor="transparent" onPress={() => Linking.openURL(`tel: ${item.contactnumber}`)}>
                        {checkEven ? 
                            <Image source={require('./../assets/images/phone_white.png')} width={22} />
                            :
                            <Image source={require('./../assets/images/phone_color.png')} width={22} />
                        }
                    </TouchableHighlight>
                </View>
            )            
        }    
    }

    _searchInput = (value) => {
        this.setState({
            searchKey: value
        })
    }

    render(){
        var searchResult = '';
        if(this.state.searchResult.length != 0){
            searchResult = <FlatList data={this.state.searchResult} renderItem={this._renderItem} numColumns={this.state.mode == 'product' ? 2 : 1} style={styles._productWrap}/>
        } else {
            searchResult = <View style={styles._blankSearch}>
                <Image source={require('./../assets/images/logo_place.png')} width={90} style={{opacity: .6}}/>
            </View>
        }

        var searchPlaceholder = ''
        if(this.state.mode == 'product'){
            searchPlaceholder = 'Search Products'
        } else if(this.state.mode == 'dealer'){
            searchPlaceholder = 'Search Dealers'
        } else {
            searchPlaceholder = 'Search Display Center'
        }

        return(
            <View style={styles._mainWrap}>
                <View style={styles._header}>
                    <View>
                        <TouchableHighlight style={styles._menu} onPress={() => this.props.navigation.goBack()} underlayColor="transparent">
                            <Image source={require('./../assets/images/back_icon.png')} width={18} />
                        </TouchableHighlight>
                    </View>
                    <View style={styles._searchWrap}>
                        <TextInput placeholder={searchPlaceholder} style={styles._searchInput} placeholderTextColor="white" value={this.state.searchKey} onChangeText={value => this._searchInput(value)} onSubmitEditing={() => this._startSearch()} autoFocus={true}/>

                        {this.state.searchKey.length != 0 ? 
                            <TouchableHighlight style={styles.searchIconWrap} onPress={() => this._searchResult()}>
                                <Image source={require('./../assets/images/search_icon.png')} width={18}/>
                            </TouchableHighlight>
                            :
                            <></>
                        }
                    </View>
                </View>
                <ImageBackground style={{width: '100%', flex: 1}} source={require('./../assets/images/plane_bg.jpg')}>  
                    {this.state.dataLoading ?   
                        <View style={styles._blankSearch}>
                            <ActivityIndicator size={50} color="white"/>
                        </View>
                        :
                        searchResult
                    }
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _mainWrap: {
        backgroundColor: '#333', flex: 1
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    _searchInput: {
        borderBottomColor: 'white', borderBottomWidth: 1, color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 12, paddingLeft: 0
    },
    _searchWrap: {
        marginRight: 15, flex: 1, marginLeft: 15
    },
    _header: {
        backgroundColor: 'black', flexDirection: 'row', justifyContent: 'space-between'
    },
    _innerProd: {
        width: '48%', backgroundColor: '#f7f7f7', borderRadius: 10, overflow: 'hidden', alignItems: 'center', paddingVertical: 10, marginBottom: '4%', minHeight: 150
    },
    _prodName: {
        textAlign: 'center', marginTop: 5, fontSize: 11, color: '#0f1217', fontFamily: 'SinkinSans-600SemiBold', lineHeight: 19
    },
    _productWrap: {
        marginTop: 15, paddingHorizontal: 15
    },
    _blankSearch: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    },
    _noResultTxt: {
        fontFamily: 'SinkinSans-600SemiBold', color: 'white'
    },
    _secWrap: {
        width: '100%', backgroundColor: 'rgba(255, 255, 255, 0.75)', flexDirection: 'row', alignItems: 'center', marginBottom: 3
    },
    _nameWrap: {
        paddingVertical: 15, paddingHorizontal: 15, flex: 1
    },
    _nameTxt: {
        color: 'black', fontFamily: 'SinkinSans-600SemiBold', fontSize: 12, lineHeight: 20
    },
    _smlDec: {
        color: 'black', fontSize: 10, fontFamily: 'SinkinSans-400Regular', marginTop: 8, lineHeight: 18
    },
    _iconImg: {
        width: 22, height: 22
    },
    _whtBtn: {
        paddingVertical: 23, paddingHorizontal: 20
    },
    _phoneBtn: {
        paddingVertical: 23, paddingHorizontal: 20, borderLeftWidth: 1, borderColor: '#123853'
    },
    _evenWrap: {
        backgroundColor: 'rgba(0, 50, 83, 0.75)'
    },
    _evenWrapSecond: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)'
    },
    _loaderWrap: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    },
    _whiteColor: {
        color: 'white'
    },
    searchIconWrap: {
        width: 35, height: 35, position: 'absolute', right: 0, justifyContent: 'center', alignItems: 'center', top: 10
    }
})

export default Search;