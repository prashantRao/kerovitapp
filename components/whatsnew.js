import React from 'react'
import { View, Text, StyleSheet, Dimensions, TouchableHighlight, Image } from 'react-native'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import NewsList from './newsList'
import BrandCampaign from './brandCampaign'
import SocialPresence from './socialPresence'

const Logo = (props) => {
    return (
        <View style={{ flex: 1, marginRight: 40 }}>
            <TouchableHighlight onPress={() => props.navigate.navigate('Category')} underlayColor="transparent">
                <Image source={require('./../assets/images/logo.png')} style={{ width: 60, height: 29, alignSelf: 'center' }} />
            </TouchableHighlight>
        </View>
    )
}

class Whatsnew extends React.Component {
    static navigationOptions = ({navigation}) => {
        return{
            headerRight: null,
            headerTitle : <Logo navigate={navigation}/>
        }                
    }

    constructor(props){
        super(props)
        this.state = {
            index: 0,
            routes: [
                { key: 'first', title: 'Latest Updates' },
                { key: 'second', title: 'Brand Campaign' },
                { key: 'third', title: 'Social Presence' },
            ],
        }
    }    
    
    renderTabBar = props => {
        return (
            <View>
                <TabBar
                    {...props}
                    style={styles._tabBar}
                    indicatorStyle={{ height: 2, backgroundColor: 'white', opacity: .4 }}
                    renderLabel={({ route }) => (
                        <View>
                            <Text style={{ fontSize: 10, fontFamily: 'SinkinSans-400Regular', textAlign: 'center', color: 'white', paddingHorizontal: 2 }}>
                                {route.title}
                            </Text>
                        </View>
                    )}
                />
            </View>
        )
    }

    render() {
        return (
            <View style={{ backgroundColor: 'black', height: '100%', width: '100%' }}>
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: NewsList,
                        second: BrandCampaign,
                        third: SocialPresence,
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                    renderTabBar={this.renderTabBar}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _wrapper: {
        padding: 15
    },
    _titleText: {
        color: 'black', fontFamily: 'SinkinSans-400Regular', fontSize: 14, lineHeight: 22
    },
    _newsWrap: {
        marginBottom: 20
    },
    _contentWrap: {
        backgroundColor: 'white', padding: 10
    },
    _tabBar: {
        backgroundColor: '#151515'
    },
    _newsDate: {
        fontFamily: 'SinkinSans-400Regular', fontSize: 10, marginBottom: 5
    }
})

export default Whatsnew;