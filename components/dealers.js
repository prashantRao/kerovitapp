import React from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Linking, ScrollView, ImageBackground, FlatList, ActivityIndicator } from 'react-native'
import {apiPath} from './constants';
import { AppContext } from './context/AppContext';

class Dealers extends React.Component {
    static contextType = AppContext;
    
    static navigationOptions = ({navigation}) => {
        return{
            headerRight: <TouchableHighlight style={styles._search} onPress={() => navigation.navigate('Search', {searchMode: 'dealer'})}>
                            <Image source={require('./../assets/images/search_icon.png')} style={{ width: 18, height: 18 }} />
                        </TouchableHighlight>,
            headerLeft: () => <TouchableHighlight style={styles._menu} onPress={() => navigation.goBack()} underlayColor="transparent">
                            <Image source={require('./../assets/images/back_icon.png')} style={{ width: 18, height: 18 }} />
                        </TouchableHighlight>
        }
    }
    
    constructor(){
        super()
        this.state = {
            dealersList: [],
            dataLoading: true
        }
    }

    componentDidMount = async () => {
        var searchKeys = await this.props.navigation.getParam('searchKey');
        this._getDealerList(searchKeys.city, searchKeys.state, searchKeys.suburbs)
    }

    _getDealerList(city, state, suburbs){
        fetch(`${apiPath}/getDealersByLocation`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                state: state,
                city: city,
                suburbs: suburbs
            })
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                dealersList: res.data
            }, () => {
                this.setState({
                    dataLoading: false
                })
            })
        })
    }

    renderItem({item, index}){
        var checkEven = index % 2 == 0;
        var updatedContent = item.contactnumber.split('/')[0];
        return(
            <View style={[styles._secWrap, checkEven ? styles._evenWrap : <></>]}>
                <View style={styles._nameWrap}>
                    <Text style={[styles._nameTxt, checkEven ? styles._whiteColor : <></>]}>{item.dealername}</Text>
                    <Text style={[styles._smlDec, checkEven ? styles._whiteColor : <></>]}>{item.address1+' '+item.address2+' '+item.address3+' '+item.suburbs+' '+item.city+', '+item.state+' - '+item.pincode}</Text>
                </View>
                <TouchableHighlight style={styles._whtBtn} underlayColor="transparent" onPress={() => Linking.openURL(`sms:${updatedContent}`)}>
                    {checkEven ? 
                        <Image source={require('./../assets/images/message_white.png')} style={styles._iconImg} />
                        :
                        <Image source={require('./../assets/images/message_color.png')} style={styles._iconImg} />
                    }
                </TouchableHighlight>
                <TouchableHighlight style={styles._phoneBtn} underlayColor="transparent" onPress={() => Linking.openURL(`tel: ${updatedContent}`)}>
                    {checkEven ? 
                        <Image source={require('./../assets/images/phone_white.png')} style={styles._iconImg} />
                        :
                        <Image source={require('./../assets/images/phone_color.png')} style={styles._iconImg} />
                    }
                </TouchableHighlight>
            </View>
        )
    }

    render() {
        return (
            <ImageBackground source={require('./../assets/images/plane_bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode="cover">                
                {this.state.dataLoading ? 
                    <View style={styles._loaderWrap}><ActivityIndicator size={50} color="black"/></View>    
                    :
                    <View style={{paddingBottom: 80, flexGrow: 1}}>
                        <FlatList data={this.state.dealersList} renderItem={this.renderItem}/>
                        <TouchableHighlight style={styles._btmBtn} onPress={() => this.props.navigation.navigate('DisplayCenter')}>
                            <Text style={styles._btnTxt}>Check Display Centers</Text>
                        </TouchableHighlight>
                    </View>
                }                
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    _secWrap: {
        width: '100%', backgroundColor: 'rgba(255, 255, 255, 0.75)', flexDirection: 'row', alignItems: 'center', marginBottom: 3
    },
    _nameWrap: {
        paddingVertical: 15, paddingHorizontal: 15, flex: 1
    },
    _nameTxt: {
        color: 'black', fontFamily: 'SinkinSans-600SemiBold', fontSize: 12, lineHeight: 20
    },
    _smlDec: {
        color: 'black', fontSize: 10, fontFamily: 'SinkinSans-400Regular', marginTop: 8, lineHeight: 18
    },
    _iconImg: {
        width: 22, height: 22
    },
    _whtBtn: {
        paddingVertical: 23, paddingHorizontal: 20
    },
    _phoneBtn: {
        paddingVertical: 23, paddingHorizontal: 20, borderLeftWidth: 1, borderColor: '#123853'
    },
    _evenWrap: {
        backgroundColor: 'rgba(0, 50, 83, 0.75)'
    },
    _loaderWrap: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    },
    _whiteColor: {
        color: 'white'
    },
    _btmBtn: {
        backgroundColor: 'black', position: 'absolute', bottom: 15, left: 15, right: 15, paddingVertical: 15, borderRadius: 60
    },
    _btnTxt: {
        color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 12, textAlign: 'center'
    },
    _search: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    }
})

export default Dealers;