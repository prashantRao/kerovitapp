import React from 'react'
import { View, Text, ImageBackground, StyleSheet, TextInput, TouchableHighlight, KeyboardAvoidingView, ScrollView, ToastAndroid } from 'react-native'
import { apiPath } from './constants'
import Image from 'react-native-scalable-image';
import { AppContext } from './context/AppContext';

const Logo = (props) => {
    return (
        <View style={{ flex: 1, marginRight: 40 }}>
            <TouchableHighlight onPress={() => props.navigate.navigate('Category')} underlayColor="transparent">
                <Image source={require('./../assets/images/logo.png')} style={{alignSelf: 'center'}} width={60}/>
            </TouchableHighlight>
        </View>
    )
}

class QueryForm extends React.Component {
    static contextType = AppContext;
    
    static navigationOptions = ({navigation}) => {
        return{
            headerRight: null,
            headerTitle : <Logo navigate={navigation}/>,
            headerLeft: <TouchableHighlight style={styles._menu} onPress={() => navigation.goBack()} underlayColor="transparent">
                <Image source={require('./../assets/images/back_icon.png')} width={18} />
            </TouchableHighlight>
        }                
    }
    constructor() {
        super()
        this.initialState = {
            name: '',
            email: '',
            phone: '',
            city: '',
            message: '',
            validateField: false,
            formSubmitted: false,
            productQuery: ''
        }
        this.state = this.initialState;
    }

    componentDidMount(){
        var productValue = this.props.navigation.getParam('productDesc');
        if(productValue != undefined){
            this.setState({
                productQuery: productValue
            })
        } else {
            this.setState({
                productQuery: ''
            })
        }
    }

    _submitForm() {
        this.setState({
            validateField: true
        })
        if (this.state.name == '' || this.state.email == '' || this.state.phone == '', this.state.city == '' || this.state.message == '') {
            this.setState({
                validateField: true
            })
        } else {
            var formData = JSON.stringify({
                name: this.state.name, email: this.state.email, phone: this.state.phone, city: this.state.city, message: this.state.message, product: this.state.productQuery
            })
            fetch(`${apiPath}/postContacts`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    mode: this?.context?.mode
                },
                body: formData
            })
                .then(res => res.json())
                .then(res => {
                    if (res.data == 'Query Submitted Successfully!') {
                        // ToastAndroid.show('Your message has been submitted!', ToastAndroid.SHORT);
                        // this.setState(this.initialState)
                        // this.refs.input.blur()
                        this.setState({
                            formSubmitted: true
                        }, () => {
                            setTimeout(() => {
                                this.props.navigation.navigate('Support')
                            }, 3000);
                        })
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    render() {
        var pageData = ''
        if (this.state.formSubmitted) {
            pageData = <View style={styles._thanyouWrap}>
                <Image source={require('./../assets/images/smile.png')} width={80} style={{marginBottom: 25, opacity: .9}}/>
                <Text style={styles._thankTxt}>{`Thank You for\nsubmitting your query!`}</Text>
            </View>
        } else {
            pageData = <ScrollView keyboardShouldPersistTaps="always">
                <View style={styles._innerWrap}>
                    <Text style={styles._mainHead}>{`We would love\nto hear from you!`}</Text>
                    <KeyboardAvoidingView behavior="padding" enabled>
                        {this.state.productQuery != '' ?
                            <TextInput value={this.state.productQuery} style={[styles._txtInput, {opacity: .9, lineHeight: 22}]} editable={false} multiline={true}/>
                            :
                            <></>
                        }                        
                        <TextInput value={this.state.name} onChangeText={(name) => this.setState({ name })} placeholder="Name*" style={[styles._txtInput, this.state.validateField && this.state.name == '' ? styles._errorField : <></>]} placeholderTextColor="#565656" ref="input" />
                        <TextInput value={this.state.email} keyboardType="email-address" autoCapitalize='none' onChangeText={(email) => this.setState({ email })} placeholder="Email*" style={[styles._txtInput, this.state.validateField && this.state.email == '' ? styles._errorField : <></>]} placeholderTextColor="#565656" ref="input" />
                        <TextInput value={this.state.phone} maxLength={10} keyboardType="number-pad" onChangeText={(phone) => this.setState({ phone })} placeholder="Phone*" style={[styles._txtInput, this.state.validateField && this.state.phone == '' ? styles._errorField : <></>]} placeholderTextColor="#565656" ref="input" />
                        <TextInput value={this.state.city} onChangeText={(city) => this.setState({ city })} placeholder="City/State*" style={[styles._txtInput, this.state.validateField && this.state.city == '' ? styles._errorField : <></>]} placeholderTextColor="#565656" ref="input" />
                        <TextInput value={this.state.message} onChangeText={(message) => this.setState({ message })} placeholder="Message*" multiline numberOfLines={5} style={[styles._txtInput, styles._textArea, this.state.validateField && this.state.message == '' ? styles._errorField : <></>]} ref="input" placeholderTextColor="#565656" />
                        <TouchableHighlight style={styles._submitBtn} onPress={() => this._submitForm()} underlayColor="white">
                            <Text style={styles._btnTxt}>Submit</Text>
                        </TouchableHighlight>
                    </KeyboardAvoidingView>
                </View>
            </ScrollView>
        }
        return (
            <ImageBackground source={require('./../assets/images/contact_bg.png')} style={styles._bg}>
                {pageData}
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    _bg: {
        backgroundColor: '#0a0a0a', width: '100%', height: '100%'
    },
    _mainHead: {
        color: 'white', fontFamily: 'SinkinSans-600SemiBold', fontSize: 28, lineHeight: 46, marginBottom: 20
    },
    _innerWrap: {
        padding: 15
    },
    _txtInput: {
        width: '100%', borderColor: '#565656', borderWidth: 1, color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 13, paddingLeft: 15, marginBottom: 20
    },
    _textArea: {
        textAlignVertical: 'top'
    },
    _submitBtn: {
        backgroundColor: 'white', width: 120, alignItems: 'center', paddingVertical: 10, borderRadius: 5
    },
    _btnTxt: {
        fontFamily: 'SinkinSans-600SemiBold', color: 'black'
    },
    _errorField: {
        borderColor: 'red'
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    _thanyouWrap: {
        alignItems: 'center', justifyContent: 'center', flex: 1, padding: 15
    },
    _thankTxt: {
        color: 'white', fontFamily: 'SinkinSans-600SemiBold', fontSize: 24, textAlign: 'center', lineHeight: 40, opacity: .7
    }
})

export default QueryForm;