import React from 'react'
import {View, Text, StyleSheet, TouchableHighlight, Linking, Dimensions, ScrollView} from 'react-native'
import Image from 'react-native-scalable-image';

const Logo = (props) => {
    return (
        <View style={{ flex: 1, marginRight: 40 }}>
            <TouchableHighlight onPress={() => props.navigate.navigate('Category')} underlayColor="transparent">
                <Image source={require('./../assets/images/logo.png')} style={{alignSelf: 'center'}} width={60}/>
            </TouchableHighlight>
        </View>
    )
}

class Support extends React.Component{
    static navigationOptions = ({navigation}) => {
        return{
            headerRight: null,
            headerTitle : <Logo navigate={navigation}/>
        }                
    }
    render(){
        return(
            <ScrollView style={styles._bg}>               
                <Image source={require('./../assets/images/support_creative.jpg')} width={Dimensions.get('window').width}/>
                <View style={styles._supportBtm}>
                    <Text style={styles._infoTxt}>For Complaints or Feedback</Text>
                    <View style={{alignItems: 'center'}}>
                        <TouchableHighlight style={styles._phoneBtn} underlayColor="white" onPress={() => Linking.openURL('tel: 1800 11 00 59')}>
                            <View style={styles._btnInner}>
                                <Image source={require('./../assets/images/phone_color.png')} width={15}/>
                                <Text style={styles._btnTxt}>1800 11 00 59</Text>
                            </View>
                        </TouchableHighlight>
                        {/* <Text style={styles._sepTxt}>Or</Text> */}
                        <TouchableHighlight style={[styles._phoneBtn, {backgroundColor: 'transparent', elevation: 0}]} underlayColor="transparent" onPress={() => this.props.navigation.navigate('QueryForm')}>
                            <Text style={[styles._btnTxt, {color: '#003253', marginLeft: 0, textDecorationLine: 'underline'}]}>Send Us Your Query</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    _bg: {
        backgroundColor: '#e0e0e0', flex: 1
    },
    _topHead: {
        color: 'white', marginTop: 20, fontSize: 16, textTransform: 'uppercase', fontFamily: 'SinkinSans-400Regular', opacity: .7
    },
    _topHeadSec: {
        color: 'white', marginTop: 5, marginBottom: 20, fontSize: 20, textTransform: 'uppercase', fontFamily: 'SinkinSans-600SemiBold'
    },
    _infoTxt: {
        color: '#193b57', fontFamily: 'SinkinSans-400Regular', marginTop: 15, textTransform: 'uppercase', fontSize: 12, textAlign: 'center'
    },
    _phoneBtn: {
        backgroundColor: 'white', paddingVertical: 10, paddingHorizontal: 10, borderRadius: 5, marginTop: 10, elevation: 2, width: '80%'
    },
    _btnTxt: {
        fontSize: 13, fontFamily: 'SinkinSans-600SemiBold', color: 'black', marginLeft: 10, textAlign: 'center'
    },
    _btnInner: {
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
    },
    _sepTxt: {
        color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 11, color: 'black', opacity: .5, textTransform: 'uppercase', marginVertical: 15, textAlign: 'center'
    },
    _supportBtm: {
        padding: 15, textAlign: 'center', paddingTop: 0
    }
})

export default Support;