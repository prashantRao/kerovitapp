import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image, SafeAreaView, StatusBar} from 'react-native';
import { useApp } from './context/AppContext';

export default function IntroPage(props){
    const {setMode} = useApp();

    const handleNavigation = (type) => {
        setMode(type);
        props.navigation.navigate('Category');
    }

    return(
        <SafeAreaView style={styles.parentWrap}>
            <StatusBar backgroundColor="black" barStyle="light-content"/>
            <TouchableOpacity style={{width: '50%'}} activeOpacity={1} onPress={() => handleNavigation('kerovit')}>
                <Image source={require('./../assets/images/left_img.jpg')} style={styles.imgStyle}/>
            </TouchableOpacity>
            <TouchableOpacity style={{width: '50%'}} activeOpacity={1} onPress={() => handleNavigation('aurum')}>
                <Image source={require('./../assets/images/right_img.jpg')} style={styles.imgStyle}/>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    parentWrap: {
        backgroundColor: 'black', flexDirection: 'row'
    },
    imgStyle: {
        width: '100%', height: '100%', resizeMode: 'cover'
    }
})