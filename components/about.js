import React from 'react'
import { View, Text, StyleSheet, ScrollView, Dimensions, ActivityIndicator } from 'react-native'
import Image from 'react-native-scalable-image';
import HTML from 'react-native-render-html';
import { apiPath } from './constants'
import { AppContext } from './context/AppContext';

class About extends React.Component {
    static contextType = AppContext;
    constructor() {
        super()
        this.state = {
            loading: true,
            data: ''
        }
    }

    componentDidMount() {
        this.fetchdata()
    }

    fetchdata = () => {
        fetch(`${apiPath}/getabout`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            }
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    data: res.data
                }, () => {
                    this.setState({
                        loading: false
                    })
                })
            })
            .catch(err => console.log(err))
    }

    render() {
        const { loading, data } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                {loading ?
                    <View style={{ flex: 1, backgroundColor: 'black', justifyContent: 'center' }}>
                        <ActivityIndicator size={20} color="white" />
                    </View>
                    :
                    <ScrollView contentContainerStyle={styles.mainWrapper}>
                        <Text style={styles.mainHeadeing}>About Us</Text>
                        <Image source={{uri: data.about_img1}} width={Dimensions.get('window').width - 30} style={styles.imgWrap} />
                        <HTML html={data.text1} imagesMaxWidth={Dimensions.get('window').width} baseFontStyle={styles.innerTxt}/>

                        <Text style={[styles.mainHeadeing, { marginTop: 20 }]}>Kajaria Ceramics</Text>
                        <Image source={{uri: data.about_img2}} width={Dimensions.get('window').width - 30} style={styles.imgWrap} />
                        <HTML html={data.text2} imagesMaxWidth={Dimensions.get('window').width} baseFontStyle={styles.innerTxt}/>

                        <Text style={[styles.mainHeadeing, { marginTop: 20 }]}>Director's Message</Text>
                        <Image source={{uri: data.about_img3}} width={200} style={styles.imgWrap} />
                        <HTML html={data.text3} imagesMaxWidth={Dimensions.get('window').width} tagsStyles={{b: styles.directorName, span: styles.directorDesg}} baseFontStyle={styles.innerTxt}/>
                    </ScrollView>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainHeadeing: {
        fontSize: 22, color: 'white', fontFamily: 'SinkinSans-600SemiBold', marginBottom: 10
    },
    mainWrapper: {
        backgroundColor: '#0c0c0c', paddingBottom: 20, padding: 15
    },
    innerTxt: {
        fontSize: 12, color: 'white', fontFamily: 'SinkinSans-400Regular', opacity: .7, lineHeight: 22, marginBottom: 20
    },
    directorName: {
        fontSize: 14, color: 'white', fontFamily: 'SinkinSans-600SemiBold', fontWeight: 'normal'
    },
    directorDesg: {
        fontSize: 11, color: 'white', fontFamily: 'SinkinSans-400Regular', opacity: .8, marginTop: 8, marginBottom: 15
    },
    imgWrap: {
        borderRadius: 10, marginTop: 5, marginBottom: 20
    }
})

export default About;