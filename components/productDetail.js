import React from 'react'
import { View, Text, ImageBackground, StyleSheet, TouchableHighlight, AsyncStorage, Dimensions, ActivityIndicator, ScrollView, Linking, Image } from 'react-native'
import { apiPath } from './constants'
import { checkStoragePermission } from './helpers/checkStoragePermission'
import RNFetchBlob from 'rn-fetch-blob'
import Share from 'react-native-share'
import ImageZoom from 'react-native-image-pan-zoom';
import { ToastAndroid } from 'react-native'
import { AppContext } from './context/AppContext'

class ProductDetail extends React.Component {
    static contextType = AppContext;
    
    constructor() {
        super()
        this.state = {
            productDetail: [],
            isLiked: false
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <TouchableHighlight style={styles._menu} onPress={() => navigation.goBack()} underlayColor="transparent">
                <Image source={require('./../assets/images/back_icon.png')} style={{ width: 18, height: 18 }} />
            </TouchableHighlight>
        }
    }

    componentDidMount() {
        let productCode = this.props.navigation.getParam('productCode')
        this._getProductDetail(productCode)        
    }

    _getProductDetail(value) {
        fetch(`${apiPath}/getProductDetails`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                product_model: value
            })
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    productDetail: res.data
                }, () => {
                    AsyncStorage.getItem('likedProducts').then(res => {
                        var stoageNewData = JSON.parse(res);
                        if(stoageNewData){
                            var isExist = stoageNewData.includes(this.state.productDetail[0].product_model)
                            if(isExist){
                                this.setState({
                                    isLiked: true
                                })
                            } else {
                                this.setState({
                                    isLiked: false
                                })
                            }
                        } else {
                            this.setState({
                                isLiked: false
                            })
                        }                    
                    })
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    _shareProduct = async (item) => {
        try {
            await checkStoragePermission()
                .then(res => {
                    if (res == 'Permission granted') {
                        RNFetchBlob.fetch('GET', item.image)
                            .then((res) => {
                                let base64Str = res.base64()
                                Share.open({
                                    message: item.product_description,
                                    url: "data:image/png;base64," + base64Str,
                                    filename: 'kerovitImage.png'
                                });
                            })
                            // Status code is not 200
                            .catch((errorMessage, statusCode) => {
                                // error handling
                            })
                    } else {
                        ToastAndroid.show('Required storage permission to share image.', ToastAndroid.SHORT);
                    }
                })
        } catch (error) {
            console.log(error)
        }
    };

    _likeProduct = async (item) => {
        var prevLiked = [];
        await AsyncStorage.getItem('likedProducts').then(res => {
            if(res){
                var storageData = JSON.parse(res)
                if(storageData.includes(item.product_model)){                    
                    this.setState({
                        isLiked: false
                    })
                    var newData = storageData.filter(data => data !== item.product_model)
                    AsyncStorage.setItem('likedProducts', JSON.stringify(newData))
                } else {
                    this.setState({
                        isLiked: true
                    })
                    prevLiked = storageData.concat(item.product_model)
                    AsyncStorage.setItem('likedProducts', JSON.stringify(prevLiked))
                }                
            } else {
                this.setState({
                    isLiked: true
                })
                prevLiked = prevLiked.concat(item.product_model)    
                AsyncStorage.setItem('likedProducts', JSON.stringify(prevLiked))
            } 
        })         
    }

    downloadPDF = (url) => {
        var number = (new Date()).valueOf();
        checkStoragePermission().then(res => {
            if(res === 'Permission granted'){
                ToastAndroid.show('Download Started!', ToastAndroid.SHORT)
                RNFetchBlob
                .config({
                    addAndroidDownloads: {
                        useDownloadManager : true,
                        notification: true,
                        mediaScannable: true,
                        description: 'File downloaded by download manager.',
                        fileCache: true,
                        path: RNFetchBlob.fs.dirs.DownloadDir + `/Kerovit${number}.pdf`
                    }
                })
                .fetch('GET', url)
                .then((res) => {
                    ToastAndroid.show('Download Complete!', ToastAndroid.SHORT)
                })
                .catch((errorMessage) => {
                    console.log(errorMessage)
                })
            } else {
                ToastAndroid.show('Permission Required!', ToastAndroid.SHORT)
            }
        })
    }

    render() {
        var pageData = '';
        if (this.state.productDetail.length == 0) {
            pageData = <View style={styles._loaderWrap}><ActivityIndicator size={50} color="black" /></View>
        } else {
            pageData = <View style={styles._detailWrap}>
                <View style={styles._prodImg}>
                    <ImageZoom cropWidth={Dimensions.get('window').width} cropHeight={300} imageWidth={200} imageHeight={200}>
                        <Image source={{ uri: this.state.productDetail[0].image, width: 200, height: 200 }} resizeMethod="resize" />
                    </ImageZoom>
                    <TouchableHighlight style={styles._queryIcon} underlayColor="#d2d2d2" onPress={() => this.props.navigation.navigate('QueryForm', {'productDesc': this.state.productDetail[0].product_description})}>
                        <Image source={require('./../assets/images/message_color.png')} style={{ width: 22, height: 22 }} />
                    </TouchableHighlight>
                    <TouchableHighlight style={styles._searchIcon} underlayColor="black" onPress={() => this._shareProduct(this.state.productDetail[0])}>
                        <Image source={require('./../assets/images/shareIcon.png')} style={{ width: 20, height: 20 }} />
                    </TouchableHighlight>
                    <TouchableHighlight style={styles._likeIcon} underlayColor="#f8f8f8" onPress={() => this._likeProduct(this.state.productDetail[0])}>
                        {this.state.isLiked ? 
                            <Image source={require('./../assets/images/like_active.png')} style={{ width: 20, height: 20 }} />
                            :
                            <Image source={require('./../assets/images/like_inactive.png')} style={{ width: 20, height: 20 }} />
                        }                        
                    </TouchableHighlight>
                </View>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View style={styles._prodDescWrap}>
                        <View style={styles._prodDescTop}>
                            <Text style={styles._prodName}>{this.state.productDetail[0].product_title}</Text>
                            <Text style={styles._prodTopInfo}>
                                {`Model No:\n${this.state.productDetail[0].product_model}`}
                            </Text>
                            <Text style={styles._prodTopInfo}>
                                {`Series:\n${this.state.productDetail[0].series}`}
                            </Text>
                        </View>
                        <View style={styles._prodBtmInfo}>
                            <Text style={styles._prodDetCont}><Text style={styles._prodDetContHead}>Description</Text> - {this.state.productDetail[0].product_description}</Text>
                            {this.state.productDetail[0].size == '' ?
                                <></>
                                :
                                <Text style={styles._prodDetCont}><Text style={styles._prodDetContHead}>Dimensions</Text> - {this.state.productDetail[0].size}</Text>
                            }
                            {this.state.productDetail[0].featured_image != '' ?
                                <React.Fragment>
                                    <Text style={styles._prodDetContHead}>Features -</Text>
                                    <Image source={{ uri: this.state.productDetail[0].featured_image, width: Dimensions.get('window').width - 30, height: 200 }} resizeMethod="resize" style={{ marginTop: 15, marginBottom: 10, resizeMode: 'contain' }} />
                                </React.Fragment>
                                :
                                <></>
                            }
                            {/* <View style={styles._featList}>
                            <FlatList
                                numColumns={4}
                                data={[
                                    { key: 'Smooth Operation', icon: './../assets/images/filter_icon.png' },
                                    { key: 'Soft Water Flow', icon: './../assets/images/filter_icon.png' },
                                    { key: 'High Longevity 5 Lac+ Cycle', icon: './../assets/images/filter_icon.png' },
                                    { key: 'High Chrome & Nickle Longevity', icon: './../assets/images/filter_icon.png' },
                                    { key: 'Maximum water saving', icon: './../assets/images/filter_icon.png' },
                                    { key: '10 Years warranty', icon: './../assets/images/filter_icon.png' },
                                    { key: 'Design Innovation', icon: './../assets/images/filter_icon.png' }
                                ]}
                                style={{ flexWrap: 'wrap', flexDirection: 'row' }}
                                renderItem={({ item }) =>
                                    <View style={[styles._featListProd, { width: (Dimensions.get('window').width - 90) / 4 }]}>
                                        <Image source={require('./../assets/images/filter_icon.png')} width={25} style={{ marginBottom: 6 }} />
                                        <Text style={styles._featureItem}>{item.key}</Text>
                                    </View>
                                }
                            />
                        </View> */}
                            <View style={styles._technicalDrawing}>
                                <Text style={styles._prodDetContHead}>Technical Drawing</Text>
                                <TouchableHighlight underlayColor="transparent" style={{ marginLeft: 15 }} onPress={() => this.downloadPDF(this.state.productDetail[0].technical_drawing)}>
                                    <Image source={require('./../assets/images/pdf.png')} width={20} />
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        }
        return (
            <View>
                <ImageBackground source={require('./../assets/images/plane_bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode="cover">
                    {pageData}
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _prodImg: {
        justifyContent: 'center', alignItems: 'center', paddingVertical: 40, backgroundColor: '#f7f7f7', height: 300
    },
    _detailWrap: {
        padding: 0, paddingBottom: 0, flex: 1
    },
    _prodDescWrap: {
        backgroundColor: '#131313', paddingBottom: 40, flex: 1
    },
    _prodName: {
        fontSize: 18, textTransform: 'uppercase', color: 'white', fontFamily: 'SinkinSans-700Bold', lineHeight: 28, width: '100%', marginBottom: 10
    },
    _prodDescTop: {
        padding: 10, paddingHorizontal: 15, flexDirection: 'row', flexWrap: 'wrap', paddingRight: 40, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#3c3c3d', marginBottom: 15, justifyContent: 'space-between'
    },
    _prodTopInfo: {
        color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 12, lineHeight: 20, width: '48%'
    },
    _prodBtmInfo: {
        paddingHorizontal: 15
    },
    _prodDetCont: {
        color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 12, lineHeight: 20, marginBottom: 15
    },
    _prodDetContHead: {
        color: 'white', fontFamily: 'SinkinSans-600SemiBold', fontSize: 12, lineHeight: 20
    },
    _featureItem: {
        color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 10
    },
    _featList: {
        flexDirection: 'row', flexWrap: 'wrap', display: 'flex'
    },
    _featListProd: {
        marginTop: 20
    },
    _loaderWrap: {
        flex: 1, justifyContent: 'center', height: '100%'
    },
    _technicalDrawing: {
        display: 'flex', marginTop: 20, flexDirection: 'row'
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    _searchIcon: {
        width: 46, height: 46, backgroundColor: 'black', position: 'absolute', right: 15, bottom: 15, borderRadius: 23, alignItems: 'center', justifyContent: 'center', elevation: 5
    },
    _queryIcon: {
        width: 46, height: 46, backgroundColor: '#d2d2d2', position: 'absolute', right: 76, bottom: 15, borderRadius: 23, alignItems: 'center', justifyContent: 'center', elevation: 5
    },
    _likeIcon: {
        width: 46, height: 46, backgroundColor: 'white', position: 'absolute', right: 15, top: 15, borderRadius: 23, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: '#ccc'
    }
})

export default ProductDetail