import React from 'react'
import { View, Text, ImageBackground, Dimensions, StyleSheet, Picker, TouchableHighlight, Image, TouchableNativeFeedback } from 'react-native'
import { apiPath } from './constants'
import { AppContext } from './context/AppContext';

const winWid = Dimensions.get('window').width;
const winHei = Dimensions.get('window').height;


class FilterPage extends React.Component {
    static contextType = AppContext;

    static navigationOptions = ({navigation}) => {
        return{
            headerRight: <TouchableHighlight style={styles._search} onPress={() => navigation.navigate('Search', {searchMode: 'dealer'})}>
                            <Image source={require('./../assets/images/search_icon.png')} style={{ width: 18, height: 18 }} />
                        </TouchableHighlight>
        }
    }
    constructor() {
        super()
        this.state = {
            selectedState: '',
            selectedCity: '',
            selectedSub: '',
            states: [],
            cities: [],
            suburbs: []
        }
    }

    componentDidMount = () => {
        this.props.navigation.addListener('willFocus', payload => {
            this.setState({
                selectedState: '',
                selectedCity: '',
                selectedSub: '',
                states: [],
                cities: [],
                suburbs: []
            })
            this._getStateList();
        })        
    }

    _getStateList = () => {
        fetch(`${apiPath}/getState`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                mode: this?.context?.mode
            }
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    states: res.data
                })
            })
    }

    _getCityList = (state) => {
        fetch(`${apiPath}/getCity`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                state: state
            })
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    cities: res.data
                })
            })
    }

    _getSubList = (city, state) => {
        fetch(`${apiPath}/getSuburbs`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                state: state,
                city: city
            })
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    suburbs: res.data
                })
            })
    }

    _getResult = () => {
        var searchData = {
            state: this.state.selectedState,
            city: this.state.selectedCity,
            suburbs: this.state.selectedSub
        }
        if (this.state.selectedState != '') {
            this.props.navigation.navigate('Dealers', { 'searchKey': searchData })
        }
    }

    render() {
        const { states, selectedState, cities, selectedCity, selectedSub, suburbs } = this.state;

        return (
            <ImageBackground source={require('./../assets/images/plane_bg.jpg')} style={styles.bg} resizeMode="cover">
                <View>
                    <Text style={[styles.headText, {fontSize: 16, marginBottom: 3, fontFamily: 'SinkinSans-400Regular', opacity: .7}]}>Find your nearest</Text>
                    <Text style={styles.headText}>Kerovit Showroom</Text>
                    <View style={styles.whiteSep}></View>
                    <View style={styles.fieldWrap}>
                        <Text style={styles.fieldLabel}>Choose State:</Text>
                        <View style={styles.pickerWrap}>
                            <Text style={styles.fieldName}>{selectedState ? selectedState : 'Select State'}</Text>
                            <Picker
                                selectedValue={selectedState}
                                style={styles.pickerField}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({
                                        selectedState: itemValue,
                                        selectedCity: '',
                                        selectedSub: ''
                                    }, () => {
                                        this._getCityList(itemValue)
                                    })
                                }>
                                <Picker.Item label='Select State' value='' />
                                {states ?
                                    states.map(res => {
                                        return (
                                            <Picker.Item label={res.state} value={res.state} />
                                        )
                                    })
                                    :
                                    <></>
                                }
                            </Picker>
                        </View>
                    </View>

                    <View style={styles.fieldWrap}>
                        <Text style={styles.fieldLabel}>Choose City:</Text>
                        <View style={styles.pickerWrap}>
                            <Text style={styles.fieldName}>{selectedCity ? selectedCity : 'Select City'}</Text>
                            <Picker
                                selectedValue={selectedCity}
                                style={styles.pickerField}
                                enabled={selectedState != '' ? true : false}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({
                                        selectedCity: itemValue,
                                        selectedSub: ''
                                    }, () => {
                                        this._getSubList(itemValue, this.state.selectedState)
                                    })
                                }>
                                <Picker.Item label='Select City' value='' />
                                {cities ?
                                    cities.map(res => {
                                        return (
                                            <Picker.Item label={res.city} value={res.city} />
                                        )
                                    })
                                    :
                                    <></>
                                }
                            </Picker>
                        </View>
                    </View>

                    <View style={styles.fieldWrap}>
                        <Text style={styles.fieldLabel}>Choose Suburbs:</Text>
                        <View style={styles.pickerWrap}>
                            <Text style={styles.fieldName}>{selectedSub ? selectedSub : 'Select Suburbs'}</Text>
                            <Picker
                                selectedValue={selectedCity}
                                style={styles.pickerField}
                                enabled={selectedCity != '' ? true : false}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ selectedSub: itemValue }, () => {
                                        this._getResult()
                                    })
                                }>
                                <Picker.Item label='Select Suburbs' value='' />
                                {suburbs ?
                                    suburbs.map(res => {
                                        return (
                                            <Picker.Item label={res.suburbs} value={res.suburbs} />
                                        )
                                    })
                                    :
                                    <></>
                                }
                            </Picker>
                        </View>
                    </View>

                    {this.state.selectedState != '' ?
                        <View style={[styles.fieldWrap, {justifyContent: 'flex-start', marginLeft: '10%', borderRadius: 5, overflow: 'hidden'}]}>
                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#666')} style={{borderRadius: 5, overflow: 'hidden'}} onPress={() => this._getResult()}>
                                <View style={styles.submitBtn}>
                                    <Text style={styles.btnText}>Submit</Text>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                        :
                        <></>
                    }
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    bg: {
        width: winWid, justifyContent: 'center', alignItems: 'center', flexGrow: 1
    },
    pickerWrap: {
        width: '80%', backgroundColor: 'white', overflow: 'hidden', borderRadius: 5
    },
    pickerField: {
        width: '100%'
    },
    fieldLabel: {
        fontFamily: 'SinkinSans-400Regular', color: 'white', alignSelf: 'flex-start', width: '80%', fontSize: 11, textTransform: 'uppercase', marginBottom: 8
    },
    fieldWrap: {
        flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', marginTop: 30
    },
    fieldName: {
        fontFamily: 'SinkinSans-400Regular', color: 'black', fontSize: 13, position: 'absolute', backgroundColor: 'white', top: 0, bottom: 0, zIndex: 10, height: 50, width: '80%', textAlignVertical: 'center', paddingLeft: 15
    },
    _search: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    headText: {
        color: 'white', fontFamily: 'SinkinSans-600SemiBold', width: '80%', marginLeft: '10%', fontSize: 22
    },
    whiteSep: {
        width: 80, height: 2, backgroundColor: 'white', marginLeft: '10%', marginTop: 15
    },
    submitBtn: {
        backgroundColor: 'black', borderRadius: 5
    },
    btnText: {
        fontFamily: 'SinkinSans-400Regular', color: 'white', fontSize: 13, paddingHorizontal: 30, paddingVertical: 8, textTransform: 'uppercase'
    }
})

export default FilterPage;