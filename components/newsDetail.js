import React from 'react'
import {View, Text, Dimensions, TouchableHighlight, StyleSheet} from 'react-native'
import {apiPath} from './constants'
import Image from 'react-native-scalable-image';
import { SliderBox } from "react-native-image-slider-box";
import { AppContext } from './context/AppContext';

class NewsDetail extends React.Component{  
    static contextType = AppContext;
      
    static navigationOptions = ({navigation}) => {
        return{
            headerLeft: <TouchableHighlight style={styles._menu} onPress={() => navigation.goBack()} underlayColor="transparent">
                            <Image source={require('./../assets/images/back_icon.png')} width={18} />
                        </TouchableHighlight>
        }
    }

    constructor(){
        super()
        this.state = {
            newsDetail: new Object,
            dataLoading: true,
            newsImages: []
        }
    }
    
    componentDidMount(){
        var id = this.props.navigation.getParam('newsId')
        this._getNewsDetail(id)
    }

    _getNewsDetail(newsId){
        fetch(`${apiPath}/newsDetail`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                id: newsId
            })
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                newsDetail: res.data,
                newsImages: res.data.images
            }, () => {
                this.setState({
                    dataLoading: false
                })
            })
        })
    }

    render(){
        var date = '';
        if(!this.state.dataLoading){
            date = this.state.newsDetail.date.split(' ')[0];
        }        
        return(            
            <View style={{backgroundColor: 'black', flexGrow: 1, padding: 15}}>
                {this.state.dataLoading ? <></> : 
                    <View>
                        <SliderBox images={this.state.newsImages} resizeMode="cover" style={{width: Dimensions.get('window').width - 30, height: 300}}/>
                        <Text style={styles._newsDate}>{date}</Text>
                        <Text style={{fontFamily: 'SinkinSans-400Regular', color: 'white', fontSize: 13, lineHeight: 24}}>{this.state.newsDetail.title}</Text>
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    },
    _newsDate: {
        fontFamily: 'SinkinSans-400Regular', fontSize: 10, marginBottom: 5, color: 'white', opacity: .7, marginTop: 15
    }
})

export default NewsDetail;