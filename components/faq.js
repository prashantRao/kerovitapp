import React from 'react'
import {TouchableHighlight, Image, View} from 'react-native'
import { WebView } from 'react-native-webview';

const Logo = (props) => {
    return (
        <View style={{ flex: 1, marginRight: 40 }}>
            <TouchableHighlight onPress={() => props.navigate.navigate('Category')} underlayColor="transparent">
                <Image source={require('./../assets/images/logo.png')} style={{ width: 60, height: 29, alignSelf: 'center' }}/>
            </TouchableHighlight>
        </View>
    )
}

class Faq extends React.Component{
    static navigationOptions = ({navigation}) => {
        return{
            headerRight: null,
            headerTitle : <Logo navigate={navigation}/>
        }                
    }
    
    render(){
        return(
            <WebView
                source={{uri: 'https://www.kerovit.com/test_folder/app/faq.html'}}
                style={{backgroundColor: 'black', padding: 10}}
            />
        )
    }
}

export default Faq;