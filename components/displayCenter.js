import React from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Linking, ScrollView, ImageBackground, FlatList, ActivityIndicator } from 'react-native'
import {apiPath} from './constants';
import { AppContext } from './context/AppContext';

class DisplayCenter extends React.Component {
    static contextType = AppContext;
    
    static navigationOptions = ({navigation}) => {
        return{
            headerRight: <TouchableHighlight style={styles._search} onPress={() => navigation.navigate('Search', {searchMode: 'displayCenter'})}>
                            <Image source={require('./../assets/images/search_icon.png')} style={{ width: 18, height: 18 }} />
                        </TouchableHighlight>
        }
    }

    constructor(){
        super()
        this.state = {
            dealersList: [],
            dataLoading: true
        }
    }

    componentDidMount(){
        this._getDealerList()
    }

    _getDealerList(){
        fetch(`${apiPath}/getDisplayCenter`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            }
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                dealersList: res.data
            }, () => {
                this.setState({
                    dataLoading: false
                })
            })
        })
    }

    renderItem({item, index}){
        var checkEven = index % 2 == 0;
        var updatedContent = item.contactnumber.split('/')[0];
        return(
            <View style={[styles._secWrap, checkEven ? styles._evenWrap : <></>]}>
                <View style={styles._nameWrap}>
                    <Text style={[styles._nameTxt, checkEven ? styles._whiteColor : <></>]}>{item.dealername}</Text>
                    <Text style={[styles._smlDec, checkEven ? styles._whiteColor : <></>]}>{item.address1+' '+item.address2+' '+item.address3+' '+item.suburbs+' '+item.city+', '+item.state+' - '+item.pincode}</Text>
                    <TouchableHighlight onPress={() => Linking.openURL(item.google_link)} style={styles._directionBtn} underlayColor="white">
                        <Text style={[styles._btnTxt, {color: 'black', fontSize: 11}]}>Directions</Text>
                    </TouchableHighlight>
                </View>
                <TouchableHighlight style={styles._whtBtn} underlayColor="transparent" onPress={() => Linking.openURL(`sms:${updatedContent}`)}>
                    {checkEven ? 
                        <Image source={require('./../assets/images/message_white.png')} style={styles._iconImg} />
                        :
                        <Image source={require('./../assets/images/message_color.png')} style={styles._iconImg} />
                    }
                </TouchableHighlight>
                <TouchableHighlight style={styles._phoneBtn} underlayColor="transparent" onPress={() => Linking.openURL(`tel: ${updatedContent}`)}>
                    {checkEven ? 
                        <Image source={require('./../assets/images/phone_white.png')} style={styles._iconImg} />
                        :
                        <Image source={require('./../assets/images/phone_color.png')} style={styles._iconImg} />
                    }
                </TouchableHighlight>
            </View>
        )
    }

    render() {
        return (
            <ImageBackground source={require('./../assets/images/plane_bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode="cover">                
                {this.state.dataLoading ? 
                    <View style={styles._loaderWrap}><ActivityIndicator size={50} color="black"/></View>    
                    :
                    <View style={{paddingBottom: 80}}>
                        <FlatList data={this.state.dealersList} renderItem={this.renderItem}/>
                        <TouchableHighlight style={styles._btmBtn} onPress={() => this.props.navigation.navigate('Dealers')}>
                            <Text style={styles._btnTxt}>Where To Buy</Text>
                        </TouchableHighlight>
                    </View>
                }                
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    _secWrap: {
        width: '100%', backgroundColor: 'rgba(255, 255, 255, 0.75)', flexDirection: 'row', alignItems: 'center', marginBottom: 3
    },
    _nameWrap: {
        paddingVertical: 15, paddingHorizontal: 15, flex: 1
    },
    _nameTxt: {
        color: 'black', fontFamily: 'SinkinSans-600SemiBold', fontSize: 12, lineHeight: 20
    },
    _smlDec: {
        color: 'black', fontSize: 10, fontFamily: 'SinkinSans-400Regular', marginTop: 8, lineHeight: 18
    },
    _iconImg: {
        width: 22, height: 22
    },
    _whtBtn: {
        paddingVertical: 23, paddingHorizontal: 20
    },
    _phoneBtn: {
        paddingVertical: 23, paddingHorizontal: 20, borderLeftWidth: 1, borderColor: '#123853'
    },
    _evenWrap: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)'
    },
    _loaderWrap: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    },
    _whiteColor: {
        color: 'white'
    },
    _btmBtn: {
        backgroundColor: 'black', position: 'absolute', bottom: 15, left: 15, right: 15, paddingVertical: 15, borderRadius: 60
    },
    _btnTxt: {
        color: 'white', fontFamily: 'SinkinSans-400Regular', fontSize: 12, textAlign: 'center'
    },
    _directionBtn: {
        backgroundColor: 'white', width: 100, paddingVertical: 6, borderRadius: 5, marginTop: 10
    },
    _search: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    }
})

export default DisplayCenter;