import React from 'react'
import {Dimensions} from 'react-native'
import {WebView} from 'react-native-webview'

class Blog extends React.Component{
    render(){
        const runFirst = `
            document.getElementById('homeheaderid').style.display = 'none';
            true;
        `;
        return(
            <WebView source={{uri: 'https://www.kerovit.com/blog/'}} style={{width: Dimensions.get('window').width, height: Dimensions.get('window').height, backgroundColor: 'black'}} injectedJavaScript={runFirst}/>
        )
    }
}

export default Blog;