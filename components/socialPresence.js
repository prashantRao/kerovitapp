import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import { withNavigation } from 'react-navigation'
import { WebView } from 'react-native-webview'

class SocialPresence extends React.Component {
    render() {
        return (
            <View style={{ backgroundColor: 'black', height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>                
                <WebView 
                    source={{uri: 'https://www.kerovit.com/test_folder/app/kerovit_social.html'}}
                    style={{backgroundColor: 'black', width: Dimensions.get('window').width - 30, height: 400, marginTop: 10}}
                    domStorageEnabled={true}
                />
            </View>
        )
    }
}

export default withNavigation(SocialPresence);