import React, { createContext, useState, useContext } from 'react';

const AppContext = createContext();

function AppProvider({children}){
    const [mode, setMode] = useState('hello');    
    return (
        <AppContext.Provider value={{mode, setMode}}>
            {children}
        </AppContext.Provider>
    )
};

const useApp = () => useContext(AppContext);

export {AppProvider, useApp, AppContext};