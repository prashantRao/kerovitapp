import React from 'react'
import { View, Image, ImageBackground, StyleSheet, ScrollView, TouchableHighlight, Text, Animated } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { apiPath } from './constants'

class Category extends React.Component {
    constructor() {
        super()
        this.state = {
            categories: [],
            dataLoading: true,
            animatedValue: new Animated.Value(-50)
        }
    }

    componentDidMount() {
        this._getCategories();
        this.startAnimation()
    }

    _getCategories() {
        fetch(`${apiPath}/getCategories`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    categories: res.data
                }, () => {
                    this.setState({
                        dataLoading: false
                    })
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    _goToProduct(id) {
        this.props.navigation.navigate('Series', {category: id})
    }

    startAnimation() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animatedValue, {
                    toValue: 220,
                    duration: 1500
                })
            ]),
            {
                iterations: 10
            }
        ).start()
    }

    render() {
        var categoriesData = '';
        if (this.state.dataLoading == true) {
            categoriesData =
                <View style={styles._loaderWrap}>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['black', '#333', 'black']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                </View>
        } else {
            categoriesData = this.state.categories.map(result => {
                return (
                    <TouchableHighlight onPress={() => this._goToProduct(result.category_name)} underlayColor="transparent">
                        <View style={styles._prdWrap}>
                            <Image source={{ uri: result.cat_img }} style={styles._prdImg} />
                            <Text style={styles._prdName}>{result.category_name}</Text>
                        </View>
                    </TouchableHighlight>
                )
            })
        }
        return (
            <ImageBackground source={require('./../assets/images/1st_bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode="cover">
                <ScrollView>
                    <View style={styles._wrap}>
                        {categoriesData}
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    _wrap: {
        width: 200, paddingTop: 80
    },
    _prdWrap: {
        width: '100%', backgroundColor: 'black', paddingVertical: 15, alignItems: 'center', borderTopRightRadius: 12, borderBottomRightRadius: 12, marginBottom: 15, height: 120, justifyContent: 'center'
    },
    _prdImg: {
        width: 100, height: 60
    },
    _prdName: {
        color: 'white', fontSize: 13, textTransform: 'uppercase', fontFamily: 'SinkinSans-600SemiBold', paddingHorizontal: 10, textAlign: 'center', lineHeight: 20
    },
    _loadWrapper: {
        width: 200, height: 120, backgroundColor: 'black', borderTopRightRadius: 12, borderBottomRightRadius: 12, position: 'relative', overflow: 'hidden', marginBottom: 15
    },
    _loadShine: {
        position: 'absolute', top: -20, bottom: -20, width: 40, backgroundColor: 'white', transform: [{ rotate: '10deg' }]
    },
    _loaderWrap: {
        width: 200
    }
})

export default Category;