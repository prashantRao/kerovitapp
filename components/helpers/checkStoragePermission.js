import {PermissionsAndroid} from 'react-native'

export async function checkStoragePermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                'title': 'Required storage permission!',
                'message': 'Kerovit App needs access to your storage to download Photos.',
                buttonPositive: 'OK'
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            return 'Permission granted';            
        }
        else {            
            return 'Permission rejected';
        }
    } catch (err) {
        console.warn(err)
    }
}