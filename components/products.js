import React from 'react'
import { View, Text, ImageBackground, Dimensions, Image, StyleSheet, TouchableHighlight, ScrollView, Animated, FlatList } from 'react-native'
import { Easing } from 'react-native-reanimated';
import {apiPath} from './constants';
import LinearGradient from 'react-native-linear-gradient';
import ProgressiveImage from './progressiveImage'
import { AppContext } from './context/AppContext';
// import Image from 'react-native-scalable-image';

class Products extends React.Component {
    static contextType = AppContext;
    
    constructor(props){
        super(props)
        this.initialState = {
            filterOn: false,
            startAnimate: new Animated.Value(0),
            textVisible: false,
            products: [],
            dataLoading: true,
            animatedValue: new Animated.Value(-80),
            series: '',
            seriesImage: '',
            categoryId: ''
        }
        this.state = this.initialState
    }

    static navigationOptions = ({navigation}) => {
        return{
            headerLeft: <TouchableHighlight style={styles._menu} onPress={() => navigation.goBack()} underlayColor="transparent">
                            <Image source={require('./../assets/images/back_icon.png')} style={{width: 18, height: 18}}/>
                        </TouchableHighlight>
        }
    }

    componentDidMount = () => {
        this.props.navigation.addListener('willFocus', payload => {
            let seriesName = this.props.navigation.getParam('series')
            let categoryName = this.props.navigation.getParam('category')
            this.setState({
                categoryId: categoryName,
                series: this.props.navigation.getParam('series'),
                seriesImage: this.props.navigation.getParam('image'),
                startAnimate: new Animated.Value(0),
                typeId: this.props.navigation.getParam('type')
            }, () => {
                if(this.state.typeId != undefined){
                    this._getTypeData(this.state.typeId)
                } else {
                    this._getProducts(seriesName);
                }
            })            
            this.startAnimation();
        })        
    }

    _getTypeData(typeName){
        fetch(`${apiPath}/searchByType`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                type: typeName
            })
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                products: res.data
            }, () => {
                this.setState({
                    dataLoading: false
                })
            })
        })
        .catch(err => {
            console.log(err)
        })
    }

    _getProducts(seriesName){
        fetch(`${apiPath}/getProducts`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                series: seriesName
            })
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                products: res.data
            }, () => {
                this.setState({
                    dataLoading: false
                })
            })
        })
        .catch(err => {
            console.log(err)
        })
    }

    _openFilter(){
        this.setState({
            filterOn: !this.state.filterOn
        }, () => {
            Animated.timing(
                this.state.startAnimate,
                {
                    toValue: this.state.filterOn ? 250 : 0,
                    duration: 150,
                    easing: Easing.linear,
                    delay: 0
                }
            ).start()
        })
    }

    startAnimation() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animatedValue, {
                    toValue: 220,
                    duration: 1500
                })
            ]),
            {
                iterations: 10
            }
        ).start()
    }

    _goToDetail(result){
        this.props.navigation.navigate('ProductDetail', {productCode: result.product_model})
    }

    _goToSeriesList(){
        this.props.navigation.navigate('SeriesList', {categoryName: this.state.categoryId})        
    }

    _goToTypeList(){
        this.props.navigation.navigate('TypeList', {categoryName: this.state.categoryId})        
    }

    _renderItem = ({item, index}) => {
        var checkEven = index % 2 == 0;
        return(
            <View style={[styles._innerProd, {marginRight: checkEven ? '4%' : 0}]}>
                <TouchableHighlight onPress={() => this._goToDetail(item)} underlayColor="transparent">
                    <View style={{display: 'flex', alignItems: 'center', paddingHorizontal: 10}}>
                        <Image source={{uri: item.image, width: 110, height: 110}} style={{resizeMode: 'contain'}} resizeMethod="resize"/>
                        <Text style={styles._prodName}>{`Model No:\n${item.product_model}`}</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }

    render() {
        var productData = '';
        if (this.state.dataLoading == true) {
            productData =
                <View style={styles._loaderWrap}>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['white', '#ccc', 'white']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['white', '#ccc', 'white']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['white', '#ccc', 'white']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['white', '#ccc', 'white']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                    <View style={styles._loadWrapper}>
                        <Animated.View style={{ left: this.state.animatedValue, height: '100%' }}>
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['white', '#ccc', 'white']} style={[styles._loadShine]} />
                        </Animated.View>
                    </View>
                </View>
        } else {
            productData = <FlatList data={this.state.products} renderItem={this._renderItem} numColumns={2}/>
        }
        return (
            <ImageBackground source={require('./../assets/images/plane_bg.jpg')} style={{ width: '100%', height: '100%' }} resizeMode="cover">
                <ScrollView removeClippedSubviews={true}>
                    <View style={styles._mainWrap}>
                        {this.state.typeId ? <></> : 
                            <View style={styles._catHead}>                            
                                <View style={{backgroundColor: '#f7f7f7', alignItems: 'center', height: 300, justifyContent: 'center'}}>
                                    <Image source={{uri: this.state.seriesImage, width: '100%', height: 300}} style={{resizeMode: 'cover'}} resizeMethod="resize"/>
                                </View>
                                <Text style={styles._catName}>{this.state.series}</Text>
                            </View>
                        }                        
                        <View style={styles._prodWrap}>
                            {productData}
                        </View>
                    </View>
                </ScrollView>
                {this.state.typeId ? <></> : 
                    <View style={styles._btmOptions}>                    
                        <Animated.View style={[styles._filterOpt, {width: this.state.startAnimate}]}> 
                            <TouchableHighlight style={[styles._btnWrap]} onPress={() => this._goToSeriesList()}>
                                <View style={styles._btnInner}>
                                    <Text style={styles._btnTxt}>{`Search by\nSeries`}</Text>                            
                                </View>
                            </TouchableHighlight>
                            <View style={{justifyContent: 'center'}}>
                                <Text style={{color: 'white', fontSize: 20}}>|</Text>
                            </View>
                            <TouchableHighlight style={styles._btnWrap} onPress={() => this._goToTypeList()}>
                                <View style={styles._btnInner}>
                                    <Text style={styles._btnTxt}>{`Search by\ntype`}</Text>                            
                                </View>
                            </TouchableHighlight>
                        </Animated.View>
                        <TouchableHighlight style={styles._filterBtn} onPress={() => this._openFilter()}>
                            <Image source={require('./../assets/images/filter_icon_large.png')} style={{width: 20, height: 20}}/>
                        </TouchableHighlight>
                    </View>
                }
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    _prodWrap: {
        width: '100%', flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, marginTop: 10, flexWrap: 'wrap'
    },
    _innerProd: {
        width: '48%', backgroundColor: '#f7f7f7', borderRadius: 10, overflow: 'hidden', alignItems: 'center', paddingVertical: 10, marginBottom: '4%', minHeight: 150
    },
    _prodName: {
        textAlign: 'center', marginTop: 5, fontSize: 11, color: '#0f1217', fontFamily: 'SinkinSans-600SemiBold', lineHeight: 19
    },
    _catHead: {
        position: 'relative'
    },
    _catName: {
        color: 'white', fontSize: 22, lineHeight: 32, fontFamily: 'SinkinSans-700Bold', textTransform: 'uppercase', textAlign: 'center', paddingHorizontal: 15, paddingTop: 12
    },
    _btmOptions: {
        position: 'absolute', bottom: 15, flexDirection: 'row', justifyContent: 'flex-end', right: 15, left: 15
    },
    _btnWrap: {
        width: '48%'
    },
    _btnTxt: {
        color: 'white', fontSize: 11, marginLeft: 6, fontFamily: 'SinkinSans-300Light', textAlign: 'center', lineHeight: 17, height: 35
    },
    _btnInner: {
        flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
    },
    _mainWrap: {
        paddingBottom: 50
    },
    _filterBtn: {
        width: 50, height: 50, backgroundColor: '#182132', borderRadius: 25, justifyContent: 'center', alignItems: 'center', elevation: 8
    },
    _filterOpt: {
        justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'rgba(24,33,50,0.8)', borderTopLeftRadius: 125, borderBottomLeftRadius: 125, paddingRight: 10, paddingLeft: 0, marginRight: -22, height: 50, overflow: 'hidden'
    },
    _loadWrapper: {
        width: '48%', height: 150, backgroundColor: 'white', borderRadius: 10, position: 'relative', overflow: 'hidden', marginBottom: 15
    },
    _loadShine: {
        position: 'absolute', top: -20, bottom: -20, width: 80, backgroundColor: 'white', transform: [{ rotate: '10deg' }]
    },
    _loaderWrap: {
        width: '100%', flexDirection: 'row', justifyContent: 'space-between', display: 'flex', flexWrap: 'wrap'
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    }
})

export default Products