import React from 'react'
import { View, Text, ImageBackground, TouchableHighlight, ActivityIndicator, FlatList, StyleSheet, Dimensions, ToastAndroid } from 'react-native'
import { apiPath } from './constants'
import Image from 'react-native-scalable-image';
import {withNavigation} from 'react-navigation'
import {checkStoragePermission} from './helpers/checkStoragePermission'
import RNFetchBlob from 'rn-fetch-blob'
import Share from 'react-native-share'
import { AppContext } from './context/AppContext';

class NewsList extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props)
        this.state = {
            pageData: []
        }
    }

    componentDidMount() {
        this._getWhatsNew()
    }

    _getWhatsNew() {
        fetch(`${apiPath}/newsList`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            }
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    pageData: res.data
                })
            })
            .catch(err => {
                console.log(err)
            })
    }   

    _shareNews = async (item) => {
        try{
            await checkStoragePermission()
            .then(res => {
                if(res == 'Permission granted'){
                    RNFetchBlob.fetch('GET', item.featured_img)
                    .then((res) => {
                        // the conversion is done in native code
                        let base64Str = res.base64()
                        Share.open({
                            message: item.title,
                            url: "data:image/png;base64,"+base64Str,
                            filename: 'kerovitImage.png'
                        });
                    })
                    // Status code is not 200
                    .catch((errorMessage, statusCode) => {
                        // error handling
                    })
                } else {
                    ToastAndroid.show('Required storage permission to share image.', ToastAndroid.SHORT);
                }
            })
        } catch(error){
            console.log(error)
        }
    }

    _newsDetail(id) {
        this.props.navigation.navigate('NewsDetail', { newsId: id })
    }

    renderItem = ({ item }) => {
        var updateDate = item.created_at.split(' ')[0];
        if(item.type === 'news'){
            return (
                <TouchableHighlight style={styles._newsWrap} onPress={() => this._newsDetail(item.id)}>
                    <View>
                        <Image source={{ uri: item.featured_img }} width={Dimensions.get('window').width - 30} />
                        <View style={styles._contentWrap}>
                            <View style={styles._leftWrap}>
                                <Text style={styles._newsDate}>{updateDate}</Text>
                                <Text style={styles._titleText}>{item.title}</Text>                        
                            </View>
                            <View>
                                <TouchableHighlight style={styles._searchIcon} underlayColor="#b7b7b7" onPress={() => this._shareNews(item)}>
                                    <Image source={require('./../assets/images/shareIcon_black.png')} width={16}/>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight >
            )
        }        
    }

    render() {
        var pageContent = ''
        if (this.state.pageData.length != 0) {
            pageContent = <FlatList data={this.state.pageData} renderItem={this.renderItem} style={styles._wrapper} />
        } else {
            pageContent = <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size={40} color="white" /></View>
        }

        return (
            <View style={{ backgroundColor: 'black', height: '100%', width: '100%' }}>
                {pageContent}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _wrapper: {
        padding: 15
    },
    _titleText: {
        color: 'black', fontFamily: 'SinkinSans-400Regular', fontSize: 14, lineHeight: 22
    },
    _newsWrap: {
        marginBottom: 20
    },
    _contentWrap: {
        backgroundColor: 'white', padding: 10, flexDirection: 'row'
    },
    _newsDate: {
        fontFamily: 'SinkinSans-400Regular', fontSize: 10, marginBottom: 5
    },
    _leftWrap: {
        flex: 1, marginRight: 20
    },
    _searchIcon: {
        width: 36, height: 36, backgroundColor: '#d9d9d9', borderRadius: 23, alignItems: 'center', justifyContent: 'center'
    }
})

export default withNavigation(NewsList);