import React from 'react'
import {View, Text, FlatList, StyleSheet, TouchableHighlight, ActivityIndicator, Image} from 'react-native'
import {apiPath} from './constants'
import { AppContext } from './context/AppContext';

class TypeList extends React.Component{
    static contextType = AppContext;

    static navigationOptions = ({navigation}) => {
        return{
            headerLeft: <TouchableHighlight style={styles._menu} onPress={() => navigation.goBack()} underlayColor="transparent">
                            <Image source={require('./../assets/images/back_icon.png')} style={{width: 18, height: 18}} />
                        </TouchableHighlight>
        }
    }

    constructor(props){
        super(props)
        this.state = {
            seriesData: [],
            dataLoading: true,
            category: ''
        }
    }

    componentDidMount = async () => {
        var categoryId = await this.props.navigation.getParam('categoryName');
        this._getSeriesData(categoryId);
        this.setState({
            category: categoryId
        })
    }

    _getSeriesData(categoryName){
        fetch(`${apiPath}/getType`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                mode: this?.context?.mode
            },
            body: JSON.stringify({
                cat_name: categoryName
            })
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                seriesData: res.data
            }, () => {
                this.setState({
                    dataLoading: false
                })
            })
        })
        .catch(err => {
            console.log(err)
        })
    }

    _gotoProducts(result) {
        this.props.navigation.navigate('Products', {type: result.type})
    }

    _renderItem = ({item}) => {
        return(
            <TouchableHighlight underlayColor='black' onPress={() => this._gotoProducts(item)}>
                <Text style={styles._listBtn}>{item.type}</Text>
            </TouchableHighlight>
        )
    }

    render(){
        var pageData = ''
        if(!this.state.dataLoading){
            pageData = <FlatList data={this.state.seriesData} renderItem={this._renderItem} style={styles._listWrap}/>
        } else {
            pageData = <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={40} color="white"/></View>
        }
        return(
            <View style={styles._bg}>
                <Text style={styles._topHead}>Search By Type:</Text>                
                {pageData}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    _bg: {
        backgroundColor: 'black', flex: 1
    },
    _topHead: {
        color: 'white', fontFamily: 'SinkinSans-600SemiBold', padding: 15,
    },
    _listBtn: {
        color: 'white', fontFamily: 'SinkinSans-400Regular', paddingVertical: 15, borderBottomWidth: 1, borderColor: 'rgba(255,255,255,0.2)', opacity: .8, lineHeight: 22
    },
    _listWrap: {
        padding: 15, paddingTop: 0
    },
    _menu: {
        paddingLeft: 15, paddingRight: 15, height: 60, justifyContent: 'center'
    }
})

export default TypeList;